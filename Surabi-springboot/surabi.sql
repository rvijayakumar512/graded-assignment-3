CREATE SCHEMA `surabi` ;

CREATE TABLE `surabi`.`login` (
  `userid` INT NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `userpassword` VARCHAR(45) NOT NULL,
  `userrole` VARCHAR(45) NOT NULL,
  `islogin` INT NOT NULL,
  PRIMARY KEY (`userid`));


insert into surabi.login values(1,"vijay","vijay123","admin",0);
insert into surabi.login values(2,"kumar","kumar123","user",0);
insert into surabi.login values(3,"kamal","789kamal","user",0);
insert into surabi.login values(4,"ziva","ziva567","user",0);
insert into surabi.login values(5,"keerthi","0123skeerthi","user",0);
insert into surabi.login values(6,"ram","ramky00","user",0);


CREATE TABLE `surabi`.`itemlist` (
  `itemno` INT NOT NULL,
  `itemname` VARCHAR(45) NOT NULL,
  `itemcost`  DECIMAL(5,2) NOT NULL,
  `itemclassification` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`itemno`));



insert into surabi.itemlist values(101,"Dosa",80.0,"veg");
insert into surabi.itemlist values(102,"Chappati",50.0,"veg");
insert into surabi.itemlist values(103,"Paratta",20.0,"veg");
insert into surabi.itemlist values(104,"Poori",60.0,"veg");
insert into surabi.itemlist values(105,"Veg fried rice",80.0,"veg");
insert into surabi.itemlist values(106,"Mushrooom fried rice",85.50,"veg");
insert into surabi.itemlist values(107,"Paneer fried rice",85.50,"veg");
insert into surabi.itemlist values(108,"Veg noodles",80.0,"veg");
insert into surabi.itemlist values(109,"Mushroom noodles",85.50,"veg");
insert into surabi.itemlist values(110,"Paneer noodles",85.50,"veg");
insert into surabi.itemlist values(111,"Veg briyani",85.0,"veg");
insert into surabi.itemlist values(112,"Chicken briyani",120.0,"nonveg");
insert into surabi.itemlist values(113,"Mutton briyani",180.0,"nonveg");
insert into surabi.itemlist values(114,"Egg briayni",100.0,"nonveg");
insert into surabi.itemlist values(115,"Fish fry",120.0,"nonveg");
insert into surabi.itemlist values(116,"Pepper Chicken",100.0,"nonveg");
insert into surabi.itemlist values(117,"Mutton fry",150.0,"nonveg");
insert into surabi.itemlist values(118,"Egg omlette",20.50,"nonveg");
insert into surabi.itemlist values(119,"Coke",25.0,"soft drinks");
insert into surabi.itemlist values(120,"Pepsi",25.0,"soft drinks");
insert into surabi.itemlist values(121,"Sprite",20.0,"soft drinks");
insert into surabi.itemlist values(122,"Vanilla scoop",40.0,"dessert");
insert into surabi.itemlist values(123,"Chocolate scoop",50.0,"dessert");
insert into surabi.itemlist values(124,"Butterscotch scoop",60.0,"dessert");
insert into surabi.itemlist values(125,"Blackcurrant scoop",60.0,"desssert");





CREATE TABLE `surabi`.`bill` (
  `id` INT NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `amount` DECIMAL(6,2) NOT NULL,
  `day` INT NOT NULL,
  `month` INT NOT NULL,
  `year` INT NOT NULL,
  PRIMARY KEY (`id`));




insert into surabi.bill values(1,"ziva",25.0,3,1,2021);
insert into surabi.bill values(2,"kamal",100.0,12,1,2021);
insert into surabi.bill values(3,"ram",480.0,8,3,2021);
insert into surabi.bill values(4,"kamal",560.0,20,5,2021);
insert into surabi.bill values(5,"ram",135.0,21,5,2021);
insert into surabi.bill values(6,"ziva",250.0,2,6,2021);
insert into surabi.bill values(7,"keerthi",1900.0,5,6,2020);



