package com.assignment.Surabispringboot.Model;

import javax.persistence.*;


//Mapping to database table name login
//Adding all queries to carry out in this table using named native query
@Entity
@Table (name="Login")
@NamedQuery(name = "User.login", query = "select u from User u where u.username = ?1 and u.userpassword = ?2 and u.userrole=?3")
@NamedQuery(name = "User.getUser", query = "select u from User u where u.userid = ?1 ")
@NamedQuery(name = "User.getAllUser", query = "select u from User u where u.userrole = ?1")
@NamedQuery(name = "User.getUserRegister", query = "select u from User u where u.username = ?1 and u.userpassword=?2")
@NamedNativeQuery(name = "User.getUserId", query = "Select * from login order by userid DESC limit 1 ",resultClass=User.class)

public class User {
	//id is primary key annotation
	//column are normal attributes in sql table 
	@Id
	@Column(name = "userid")
	private int userid;
	@Column(name ="username")
	private String username;
	@Column(name ="userpassword")
	private String userpassword;
	@Column(name ="userrole")
	private String userrole;
	@Column(name ="islogin")
	private int islogin;

	//getters and setters
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserpassword() {
		return userpassword;
	}
	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}
	public String getUserrole() {
		return userrole;
	}
	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}
	public int getIslogin() {
		return islogin;
	}
	public void setIslogin(int islogin) {
		this.islogin = islogin;
	}

	//tostring method
	@Override
	public String toString() {
		return "User [userid=" + userid + ", username=" + username + ", userpassword=" + userpassword + ", userrole="
				+ userrole + "]";
	}
}