package com.assignment.Surabispringboot.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;


//Mapping to database table name item
//Adding all queries to carry out in this table using named native query
@Entity
@Table (name="Itemlist")
@NamedNativeQuery(name = "Item.display", query = "select * from itemlist where itemclassification = ?1",resultClass=Item.class)
@NamedNativeQuery(name = "Item.add", query = "select * from itemlist where itemno = ?1 and itemname= ?2 and itemclassification= ?3",resultClass=Item.class)
@NamedNativeQuery(name = "Item.checkdb", query = "select * from itemlist where itemno = ?1",resultClass=Item.class)
public class Item {
	//id is primary key annotation
	//column are normal attributes in sql table 
	@Id
	@Column(name = "itemno")
	private int no;
	@Column(name ="itemname")
	private String name;
	@Column(name ="itemcost")
	private String cost;
	@Column(name ="itemclassification")
	private String classification;

	//getters and setters
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}

	//tostring method
	@Override
	public String toString() {
		return "Item [no=" + no + ", name=" + name + ", cost=" + cost + ", classification=" + classification + "]";
	}

}