package com.assignment.Surabispringboot.Controller;
import java.util.ArrayList;
import java.util.List;

import com.assignment.Surabispringboot.Model.Bill;

//This class and its object is used in Billcontroller.java file
//this is used to return total sale along with the corresponding data of users and their bill amount
public class CombineObject {

	protected double totalbill=0;
	protected List<Bill> list=new ArrayList<>();

	public CombineObject(double totalbill, List<Bill> list) {
		super();
		this.totalbill = totalbill;
		this.list = list;
	}
	public double getTotalbill() {
		return totalbill;
	}
	public void setTotalbill(double totalbill) {
		this.totalbill = totalbill;
	}
	public List<Bill> getList() {
		return list;
	}
	public void setList(List<Bill> list) {
		this.list = list;
	}

}
