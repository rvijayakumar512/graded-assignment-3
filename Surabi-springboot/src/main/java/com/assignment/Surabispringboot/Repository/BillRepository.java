package com.assignment.Surabispringboot.Repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.assignment.Surabispringboot.Model.Bill;
//extends JPA repository 
public interface BillRepository extends JpaRepository<Bill, Integer>{
	//specifying methods of named native query
	Bill getBillId();
	List<Bill> getTodayBill(String day,String month,String year);
	List<Bill> getTotalSale(String month);
	//query to insert into db
	@Modifying
	@Transactional
	@Query(value = "insert into bill values(?1,?2,?3,?4,?5,?6)", nativeQuery = true)
	void insertdb(int id,String username,String amount, String date, String month,String year);
}
