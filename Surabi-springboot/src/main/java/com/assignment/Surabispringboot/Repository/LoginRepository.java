package com.assignment.Surabispringboot.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.Surabispringboot.Model.User;
//extends JPA repository
public interface LoginRepository extends JpaRepository<User, Integer>{
	//specifying methods of named native query
	User login(String username, String userpassword, String userrole);
	User getUser(int userid);
	User getUserRegister(String username,String userpassword);
	List<User> getAllUser(String userrole);
	User getUserId();
}
