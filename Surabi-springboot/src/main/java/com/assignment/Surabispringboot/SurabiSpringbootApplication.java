package com.assignment.Surabispringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

//Main class from where spring boot application is started

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass=true) 
public class SurabiSpringbootApplication{

	public static void main(String[] args) {
		SpringApplication.run(SurabiSpringbootApplication.class, args);
	}

}
