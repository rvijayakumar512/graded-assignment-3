package com.assignment.Surabispringboot.Aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect  
@Component  
public class BillServiceAspect   
{  
	//Defining point cut for storing logging info of bill 
	@Pointcut(value= "execution(* com.assignment.Surabispringboot.Controller.BillController.*(..)) and args(user)")  
	private void logForBill()   
	{   
	}

	//Declaring the around advice that is applied before and after the method matching with a pointcut expression  
	@Around(value= "logForBill()")  
	public Object aroundAdvice(ProceedingJoinPoint jp) throws Throwable   
	{  
		//Printing method name ..class name... and logging information to console
		String method=jp.getSignature().getName();
		String classname=jp.getTarget().getClass().toString();
		System.out.println("**************************From aspect class before requesting*******************************" + "\nmethod:  "+ method +  "class: " + classname +"\n");  
		Object result = null;
		try   
		{  
			result= jp.proceed();
			System.out.println(result);
		}   
		catch(Exception e)
		{  

		}
		System.out.println("\n**************************From aspect class after requesting*******************************\n\n");
		return result;
	}  
}  
