
# Graded Assignment-3

Run the surabi.sql file placed inside Surabi-springboot folder to create schema and table and also to initialise with few values. 

Kindly run this file in mysql command line before proceeding with project as there is one minor change to database added for logout functionality and hence it may differ from previous assignment schema.  

Refer to the below link for details of routes to get a clear picture for executing in postman app.

<https://documenter.getpostman.com/view/16959408/TzzAKbd9>